let c=document.getElementById("canvas").getContext("2d");
let hammer=new Hammer(document.getElementById("canvas"));

hammer.get("swipe").set({direction: Hammer.DIRECTION_ALL})
hammer.on('swipeleft swiperight swipedown swipeup',(e)=>{
    if(!game.isOver) snake.changeDir(e.type);
});

document.getElementById("canvas").addEventListener('dblclick',()=>{
    if(!game.isOver) game.pause();
});
document.getElementById("canvas").addEventListener('click',()=>{
    if(game.isOver){
        game=new Game();
        food=new Food(); 
        snake=new Snake();
    }else if(game.isPaused){
        game.isPaused=false;
    }
});

document.addEventListener('keydown',(e)=>{
    if(!game.isOver) snake.changeDir(e.key);
    else if(e.key!=""){
        game=new Game();
        food=new Food(); 
        snake=new Snake();
    }
});
let game=new Game();
let food=new Food();
let snake=new Snake();

setInterval(()=>{
    if(!game.isPaused && !game.isOver){
        snake.update(); 
        food.draw();
        game.overlay();
    }
},1000/10);
